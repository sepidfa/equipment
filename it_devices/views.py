from django.shortcuts import render
from django.http import HttpResponse
from it_devices.models import Device, User


def home(request):
    return render(request, 'home.html', {'name': 'sepid'})


def available(request, pk):
    # devices = Device.objects.all()
    if Device.objects.filter(pk=pk):
        return HttpResponse('The device is available')
    else:
        return HttpResponse('It is not  available')


def login(request):
    if request.method == 'POST':
        try:
            user = User.objects.get(user_name=request.POST['user_name'], password=request.POST['password'])
            if user:
                user_name = request.session['user_name']
                msg_ok = 'Welcome'
                return HttpResponse(msg_ok+user_name)
            else:
                msg_not_ok = 'The username or password is not correct'
                return HttpResponse(msg_not_ok)
        except:
            msg_not_ok_total = 'There is not such a username'
            return HttpResponse(msg_not_ok_total)

    return render(request, 'login.html', {'msg': 'Welcome\t', 'name': 'sepid'})


def add(request):
    val1 = int(request.POST["num1"])
    val2 = int(request.POST["num2"])
    result = val1+val2
    return render(request, 'result.html', {'num1': val1, 'num2': val2, 'result': result})


def logout(request):
    request.session.flush()
    return render(request, 'home.html')
