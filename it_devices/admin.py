from django.contrib import admin
#from core.models import Person, User, SalesMan
from it_devices.models import Device, Factor,Person, User, SalesMan

admin.site.register(Device)
admin.site.register(Person)
admin.site.register(User)
admin.site.register(SalesMan)
admin.site.register(Factor)
