from django.urls import path

from it_devices import views

urlpatterns = [
    path('', views.home, name='home'),
    path('<int:pk>', views.available, name='available'),
    path('login/', views.login, name='login'),
    path('add/', views.add, name='add')
]