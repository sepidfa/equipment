from django.apps import AppConfig


class ItDevicesConfig(AppConfig):
    name = 'it_devices'
