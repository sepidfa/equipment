from django.db import models

class Person(models.Model):
    user = {
        ('AD', 'admin'),
        ('OR', 'ordinary')
    }

    first_name = models.CharField(max_length=100, verbose_name='First Name of the user', null=False, blank=False,
                                  default=None)
    last_name = models.CharField(max_length=100, verbose_name='Last Name of the user', null=False, blank=False,
                                 default=None)
    #user_name = models.CharField(max_length=30, verbose_name='User Name', null=True,blank=True, default=None)
    #password = models.CharField(max_length=100, verbose_name='Password', null=True, blank=True, default=None)
    #user_type = models.CharField(max_length=4, choices=user)
    email = models.EmailField(max_length=254, verbose_name="User's Email Address", null=False, blank=False,
                              default=None)
    work_phone = models.CharField(max_length=15, verbose_name='The Work Place phone', null=False, blank=True,
                                  default=None)
    mobile_no = models.CharField(max_length=14, verbose_name='The Mobile Number', null=False, blank=False,
                                 default=None)
    address = models.TextField(max_length=500, verbose_name='The address of User', null=False, blank=False,
                               default=None)

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)

    # class Meta:
    #     abstract = True


class User(Person):
    user = {
        ('AD', 'admin'),
        ('OR', 'ordinary')
    }
    user_name = models.CharField(max_length=30, verbose_name='User Name', null=True, blank=True, default=None)
    password = models.CharField(max_length=128, verbose_name='Password', null=True, blank=True, default=None)
    user_type = models.CharField(max_length=4, choices=user)

    def __str__(self):
        return self.user_name


class SalesMan(Person):
    store_name = models.CharField(max_length=50, verbose_name='Name of the Store', null=False, blank=False,
                                  default=None)
    web_site = models.CharField(max_length=100, verbose_name='Web Site', null=True, blank=True)

    def __str__(self):
        return self.store_name

class Device (models.Model):
    various_kind = {
        (1, 'CPU'),
        (2, 'RAM'),
        (3, 'Graphic Card'),
        (4, 'Mouse'),
        (5, 'Keyboard'),
        (6, 'Mother Board'),
        (7, 'NIC'),
        (8, 'FAN'),
        (9, 'Printer Cartridge'),
        (10, 'Hard Drive')
    }
    brand_is = {
        ('hp', 'HP'),
        ('wd', 'Western Digital'),
        ('int', 'Intel'),
        ('amd', 'AMD'),
        ('sg', 'Seagate'),
    }

    name = models.CharField(max_length=100, verbose_name='Name of Device', null=False, blank=False)
    the_type = models.IntegerField(choices=various_kind, verbose_name='Type of Device', default=1, null=True)
    serial_number = models.CharField(max_length=100, verbose_name='Serial Number of Device', null=False, blank=True)
    brand = models.CharField(max_length=5, choices=brand_is, verbose_name='Manufacture', null=False, default='hp')
    date_of_made = models.IntegerField(verbose_name='Date of Production', null=True, blank=True)
    price = models.IntegerField(verbose_name='Price of the device', null=False, blank=False)
    exist = models.BooleanField(verbose_name='Available in warehouse right now', null=False, blank=False)
    number_of_device = models.IntegerField(verbose_name='How many devices are available in warehouse at the moment?',
                                           null=False)
    users_ordered = models.ManyToManyField('User', related_name='devices_ordered')
    salesmans_saled = models.ManyToManyField('SalesMan', related_name='devices_saled')

    def __str__(self):
        return self.name


class Factor(models.Model):
    serial = models.CharField(max_length=15, verbose_name='Serial Number of Factor', null=False, blank=False,
                              default=None)
    date_of_order = models.DateField(max_length=8, verbose_name='The Date of Submitting', auto_now=True, null=False,
                                     blank=False)
    user = models.ForeignKey(User, verbose_name='Factor.user', null=False, blank=False, default='Admin',
                             on_delete=models.CASCADE, related_name='user')
    salesman = models.ForeignKey(SalesMan, verbose_name='The Name of Seller', on_delete=models.CASCADE, null=False,
                                 blank=False, default=None, related_name='salesman')

    def __str__(self):
        return 'SN: {}  ;  Date: {}'.format(self.serial, self.date_of_order)


# class Ordering(models.Model):
#     device = models.ForeignKey(Device, verbose_name='The Device Submitted in The Factor', on_delete=models.CASCADE)
#     number_of_devices = models.CharField(max_length=100, verbose_name='How Many Is Ordered')
#     price_of_each_device = models.ForeignKey(Device, verbose_name='The Price of Each Item', on_delete=models.CASCADE,
#                                              related_name='')
#     total_number_of_orders = models.CharField(max_length=20, verbose_name='Total number of Orders', null=False,
#                                               blank=True, default=0)
#     total_price = models.CharField(max_length=20, verbose_name='Total Price', null=False, blank=True, default=0)
